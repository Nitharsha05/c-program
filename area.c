#include <stdio.h>

int main()
{
    int r;
    float area;
    const double pi=3.14;
    printf("Enter the value of r : ");
    scanf("%d",&r);
    area=pi*r*r;
    printf("The value of radius is %d \n",r);
    printf("The area of disk is %f",area);
    return 0;
}
