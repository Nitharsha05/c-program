#include <stdio.h>
int main()
{
    int x,y,*a,*b,t;
    printf("Enter the value of x : ");
    scanf("%d",&x);
    printf("Enter the value of y : ");
    scanf("%d",&y);
    printf("Before swapping, value of x,y is %d,%d\n",x,y);
    a=&x;
    b=&y;

    t=*a;
    *a=*b;
    *b=t;

    printf("After swapping value of x,y is %d,%d\n",x,y);
    return 0;
}
